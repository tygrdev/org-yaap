#+TITLE: Org Yaap tests

* Yaap test alert before 0
  SCHEDULED: <2021-10-24 Sun 08:23>
  :PROPERTIES:
  :CUSTOM_ID: alert-before-0
  :END:

* Yaap test custom alert before
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: custom-alert-before
  :ALERT_BEFORE: 20
  :END:

* Yaap test ignore scheduled
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: ignore-scheduled
  :END:

* Yaap test ignore deadline
  DEADLINE: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: ignore-deadline
  :END:

* Yaap test todo only ignore
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: todo-only-ignore
  :END:

* TODO Yaap test todo only
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: todo-only
  :END:
* Yaap test todo only include tags                                    :alert:
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: todo-only-tag
  :END:

* DONE Yaap test include done
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: include-done
  :END:

* DONE Yaap test exclude done
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: exclude-done
  :END:

* Yaap test only tags should alert                               :test:alert:
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: only-tags-should-alert
  :END:

* Yaap test only tags should not alert                                 :test:
  SCHEDULED: <2021-10-24 Sun 08:22>
  :PROPERTIES:
  :CUSTOM_ID: only-tags-should-not-alert
  :END:

* Yaap test daily alert
  SCHEDULED: <2021-10-24 Sun>
  :PROPERTIES:
  :CUSTOM_ID: daily-alert
  :END:
* Yaap test custom alert time
  SCHEDULED: <2021-10-24 Sun>
  :PROPERTIES:
  :CUSTOM_ID: alert-time
  :ALERT_TIME: 19 20
  :END:

* Yaap test overdue interval
  SCHEDULED: <2021-10-24 Sun 08:00>
  :PROPERTIES:
  :CUSTOM_ID: overdue-interval
  :END:

* Yaap test overdue list
  SCHEDULED: <2021-10-24 Sun 09:00>
  :PROPERTIES:
  :CUSTOM_ID: overdue-list
  :ALERT_OVERDUE: 13 34
  :END:

* Yaap test diary alert
  SCHEDULED: <%%(diary-cyclic 7 9 2 2022)>
  :PROPERTIES:
  :CUSTOM_ID: diary-alert
  :END:

* Yaap test wrong day
  SCHEDULED: <2022-09-08 Thu 15:00>
  :PROPERTIES:
  :CUSTOM_ID: wrong-day
  :END:

* Yaap test plain time 5:38pm
  SCHEDULED: <2022-09-07 Wed>
  :PROPERTIES:
  :CUSTOM_ID: plain-time
  :END:

* Yaap test plain time diary sexp 8:49am
  SCHEDULED: <%%(diary-cyclic 7 9 7 2022)>
  :PROPERTIES:
  :CUSTOM_ID: plain-diary
  :END:
