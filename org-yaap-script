#!/usr/bin/env -S emacs -Q --script
;; org-yaap-script --- Example script to run org-yaap in the background. -*- mode: emacs-lisp; lexical-binding: t; -*-

;; Keep emacs batch mode from exiting.
(setq noninteractive nil)

;; Treat all file local variables as safe
(setq enable-local-variables :all)

;; Require org-yaap
(require 'org-yaap "~/.emacs.d/straight/build/org-yaap/org-yaap")

;; Set agenda files
(setq my/org-directory "~/org")
(setq org-agenda-files (list (expand-file-name "projects.org" my/org-directory)
                             (expand-file-name "IN.org" my/org-directory)))

;; Customize org-yaap
(setq org-yaap-daemon-idle-time nil
      org-yaap-include-scheduled t
      org-yaap-include-deadline t
      org-yaap-alert-title "Agenda"
      org-yaap-alert-severity nil
      org-yaap-alert-timeout -1
      org-yaap-alert-before 0
      org-yaap-daily-alert 9
      org-yaap-todo-only nil
      org-yaap-exclude-done t
      org-yaap-overdue-alerts 30
      org-yaap-include-tags nil
      org-yaap-exclude-tags nil
      org-yaap-only-tags nil)

;; Start daemon
(org-yaap-mode 1)

;;; org-yaap-script ends here
